package com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.mysql.jdbc.Connection;


@WebServlet("/signup")
public class signup extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public signup() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String privateName = "";
		String lastName = "";
		String userName = "";
		String password = "";
		String email = "";
		String phoneNumber = "";
				
		InputStream inputstream = request.getInputStream();
		char [] messagefromclient = new char[1024];
		int read = -1;
		String messageToClient = "";
 
		StringBuilder sb = new StringBuilder();

		//reading from client
	    try 
	    {
	    	BufferedReader reader = new BufferedReader(new InputStreamReader(inputstream));
	      
	    	while ((read = reader.read(messagefromclient)) > 0)
	    	sb.append(messagefromclient,0,read);
	    } 
	    
	    catch (Exception e) 
	    {
	    	e.printStackTrace();
	    }
		
		
		System.out.println("user message: "+ sb.toString());
		
		try {
			//process the message from client
			
			JSONObject jsonObject = new JSONObject(sb.toString());
			privateName = jsonObject.getString("private name");
			lastName = jsonObject.getString("last name");
			userName = jsonObject.getString("user name");
			password = jsonObject.getString("password");
			email = jsonObject.getString("email");
			phoneNumber = jsonObject.getString("phone number");
			
			//TODO: add check for input
			//TODO: generate password for this session
			
			//verificate user and build server answer
			boolean approved = insertNewUser(privateName, lastName, userName, password, email, phoneNumber);
			if (approved){
				password = Security.generateAndUpdateSessionPassword(userName);
			}
			messageToClient = buildMessageToUser(userName, password, approved);
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}
		
		//send the message to user
		PrintWriter writer= response.getWriter();
		System.out.println("server message: "+ messageToClient);
		response.setStatus(200);	
		writer.append(messageToClient);
	}
	
	
	
	private String buildMessageToUser(String userName, String password, boolean success) {
		
		JSONObject jsonObject = new JSONObject();
		
		try 
		{
			if (success)
			{
				jsonObject.put("user name", userName);
				jsonObject.put("session password", password);	//need to process temporary key, for this session
				jsonObject.put("success", true);
			}
			
			else
			{
				jsonObject.put("success", false);
				jsonObject.put("user name", userName);
				jsonObject.put("session password", password);	//need to process temporary key, for this session
			}
		} 
		
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
		
		return jsonObject.toString();
		
	}
	
	
	
	private boolean insertNewUser(String privateName, String lastName, String userName, String password, String email, String phoneNumber) {
		
		String URL = "jdbc:mysql://127.11.254.130:3306/prepay1.0";
		String USERNAME = "adminijH4iQG";
		String PASS = "wij_lXp3HHqj";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = (Connection) DriverManager.getConnection(URL,USERNAME,PASS);
			Statement stmt = conn.createStatement();
		    int rs = stmt.executeUpdate("INSERT INTO `customers`(`private_name`, `last_name`, `user_name`, `password`, `email`, `phone_number`) "
		    		+ "VALUES ('"+privateName+"',"
		    				+"'"+lastName+"',"
		    				+"'"+userName+"',"
		    				+"'"+password+"',"
		    				+"'"+email+"',"
		    				+"'"+phoneNumber+"')");
		    
		    System.out.println("insert new user func: "+rs);
		    if (rs>0){
				return true;
			}
		    conn.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();	
		}
		
		return false;
	}

}
