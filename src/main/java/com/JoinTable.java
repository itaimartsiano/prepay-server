package com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.mysql.jdbc.Connection;


@WebServlet("/JoinTable")
public class JoinTable extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public JoinTable() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("\n new join table request");
		
		String userName = "";
		String passwordFromServer = "";
		String resturantName = "";
		String nickname = "";
		
		InputStream inputstream = request.getInputStream();
		char [] messagefromclient = new char[1024];
		int read = -1;
		String messageToClient = "";
 
		StringBuilder sb = new StringBuilder();

		//reading from client
	    try 
	    {
	    	BufferedReader reader = new BufferedReader(new InputStreamReader(inputstream));
	      
	    	while ((read = reader.read(messagefromclient)) > 0)
	    	sb.append(messagefromclient,0,read);
	    } 
	    
	    catch (Exception e) 
	    {
	    	e.printStackTrace();
	    }
		
		
		System.out.println("user message: "+ sb.toString());
		
		try {
			//process the message from client
			
			JSONObject jsonObject = new JSONObject(sb.toString());
			userName = jsonObject.getString("user name");
			passwordFromServer = jsonObject.getString("password");
			resturantName = jsonObject.getString("restName");
			nickname = jsonObject.getString("nickName");
			
			boolean approved = Security.checkSessionPassword(userName, passwordFromServer);
			
			if (approved){
				messageToClient = buildMessageToUser(joinTable(resturantName, nickname,userName));
			}
			else{
				//TODO: add something todo if user is not authrized
			}
			
				
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}
		
		//send the message to user
		PrintWriter writer= response.getWriter();
		System.out.println("server message to client: "+ messageToClient);
		response.setStatus(200);	
		writer.append(messageToClient);
		
	}
	
	
	
	
	private boolean joinTable(String restName, String nickname, String userName){
		
		String URL = "jdbc:mysql://127.11.254.130:3306/prepay1.0";
		String USERNAME = "adminijH4iQG";
		String PASS = "wij_lXp3HHqj";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = (Connection) DriverManager.getConnection(URL,USERNAME,PASS);
			Statement stmt = conn.createStatement();
			//check if user already exist in order partners - if yes, check if the nickname is equal - if yes return true else false
			//check
			ResultSet res = stmt.executeQuery("SELECT `customer_user_name`, `customer_owner`,`nickname`, `restuarant_name`, `order_partners`.`order_id`"
					+ "FROM `prepay1.0`.`order_partners` join `prepay1.0`.`orders` on `order_partners`.`order_id`=`orders`.`order_id`  "
					+ "WHERE `nickname` = '"+nickname+"' and `restuarant_name` = '"+restName+"'");
			
			//if true it mean that nickname is exist in this rest
			if (res.next()){
				//check if user is already partner in this order - if yes return true. if not - insert him and return true
				
				if (checkIfUserIsAlreadyExistInRS(res, userName)){
					conn.close();
					return true;
				}else{
					res.first();
					String orderId = res.getString("order_id");
					stmt.execute("INSERT INTO `prepay1.0`.`order_partners` (`customer_user_name`, `order_id`) VALUES ('"+userName+"','"+orderId+"')");
					//TODO: what if it was not succeed
					conn.close();
					return true;
				}
				
			}else{
				conn.close();
				return false;
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();	
		}
		
		return false;
	}
	
	
	
	private boolean checkIfUserIsAlreadyExistInRS (ResultSet rs, String userName){
		
		try 
		{
			
			rs.beforeFirst();
			while (rs.next()){
				String checkedUser = rs.getString("customer_user_name");
				System.out.println("11111111111"+checkedUser);
				if (checkedUser.equals(userName)){
					return true;
				}
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	
	
	
	
	
	
	private String buildMessageToUser(boolean approved) {
		
		JSONObject jsonObject = new JSONObject();
		
		try 
		{
			if (approved)
			{
				System.out.println("approved");
				jsonObject.put("approved", true);
			}
			
			else
			{
				System.out.println("not approved");
				jsonObject.put("approved", false);
			}
		} 
		
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
		return jsonObject.toString();
		
	}

}
