package com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.log.Log;





@WebServlet("/signin")
public class signin extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
 
    public signin() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String userName = "";
		String password = "";
		
		InputStream inputstream = request.getInputStream();
		char [] messagefromclient = new char[1024];
		int read = -1;
		String messageToClient = "";
 
		StringBuilder sb = new StringBuilder();

		//reading from client
	    try 
	    {
	    	BufferedReader reader = new BufferedReader(new InputStreamReader(inputstream));
	      
	    	while ((read = reader.read(messagefromclient)) > 0)
	    	sb.append(messagefromclient,0,read);
	    } 
	    
	    catch (Exception e) 
	    {
	    	e.printStackTrace();
	    }
		
		
		System.out.println("user message: "+ sb.toString());
		
		try {
			//process the message from client
			
			JSONObject jsonObject = new JSONObject(sb.toString());
			userName = jsonObject.getString("user name");
			password = jsonObject.getString("password");
			
			boolean autorized = Security.verificateUser(userName, password);
			String passFromServer = password;
			
			if (autorized){
				passFromServer = Security.generateAndUpdateSessionPassword(userName);
			}
	
			messageToClient = buildMessageToUser(userName, passFromServer, Security.verificateUser(userName, password));
				
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}
		
		//send the message to user
		PrintWriter writer= response.getWriter();
		System.out.println("server message to client: "+ messageToClient);
		response.setStatus(200);	
		writer.append(messageToClient);
		
	}
	
	
	
	
	
	
	
	private String buildMessageToUser(String userName, String password, boolean approved) {
		
		JSONObject jsonObject = new JSONObject();
		
		try 
		{
			if (approved)
			{
				System.out.println("approved");
				jsonObject.put("user name", userName);
				jsonObject.put("password from server", password);	//need to process temporary key, for this session
				jsonObject.put("approved", true);
			}
			
			else
			{
				System.out.println("not approved");
				jsonObject.put("approved", false);
				jsonObject.put("user name", userName);
				jsonObject.put("password from server", password);	//need to process temporary key, for this session
			}
		} 
		
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
		
		return jsonObject.toString();
		
	}
	
	
	
	
	
	
	
	
	


}
