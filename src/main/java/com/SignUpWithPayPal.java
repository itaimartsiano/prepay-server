package com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;









import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.mysql.jdbc.Connection;
import com.paypal.core.Constants;
import com.paypal.core.rest.APIContext;
import com.paypal.core.rest.PayPalRESTException;
import com.paypal.sdk.openidconnect.CreateFromAuthorizationCodeParameters;
import com.paypal.sdk.openidconnect.Tokeninfo;
import com.paypal.sdk.openidconnect.Userinfo;
import com.paypal.sdk.openidconnect.UserinfoParameters;


@WebServlet("/SignUpWithPayPal")
public class SignUpWithPayPal extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private final  String clientId = "ASAskIDfU6YHF33CO9Ri2ZHdQyYT_kdF_vFsZYzhW3-ruTes1a15E_yYGyDjVcmgjNXKsTMwOFcrYB4C";
	private final String clientSecret = "EEz4vora0Rn9RqYaR2Cpjce_-J0LRUkAwYKS2hwtJ16J5u8jHaFYfLqGLdSy2k8FChih7_2Z7MCs49Pu";
	private Tokeninfo tokenInfo;
	private Map<String, String> configurationMap;
	private String messageToClient;
	

	
	
    public SignUpWithPayPal() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		System.out.println("Sign Up With Pay Pal Request");
		//read message from user
		String messageFromUser = readMessageFromUser(request);
		
		try 
		{
			JSONObject jsonObject = new JSONObject(messageFromUser);
			//get access token from auth code
			tokenInfo = getAccessTokenFromAuthCode(jsonObject.getString("json"));
			
			//get user info from access token
			Userinfo userInfo = getUserInfoFromAccessToken();
			
			System.out.println(userInfo.getVerifiedAccount());
			//TODO: change to check if user was verified with paypal
			if (true){
				//save user info to DB
				if (saveDataToDB(userInfo)){
					String password = Security.generateAndUpdateSessionPassword(userInfo.getEmail());
					messageToClient = buildMessageToUser(userInfo.getEmail(), password, true, null);
				}else{
					//TODO: build message to user why failed
				}
			}else{
				//TODO: build message that the account was not verified by paypal
			}	
		}
		
		catch (JSONException e) {
			e.printStackTrace();
		}
		
		//send the message to user
		PrintWriter writer= response.getWriter();
		response.setStatus(200);	
		writer.append(messageToClient);
		System.out.println("server message: "+ messageToClient);
	}
	
	
	
	
	
	
	
	private String readMessageFromUser(HttpServletRequest request) {
		
		InputStream inputstream;
		StringBuilder sb = new StringBuilder();
		try {
			inputstream = request.getInputStream();
			char [] messagefromclient = new char[1024];
			int read = -1;
 
			//reading from client
	    	BufferedReader reader = new BufferedReader(new InputStreamReader(inputstream));
	      
	    	while ((read = reader.read(messagefromclient)) > 0)
	    	sb.append(messagefromclient,0,read);
	    } 
	    
	    catch (Exception e) 
	    {
	    	e.printStackTrace();
	    }
		
		System.out.println("user message: "+ sb.toString());
		return sb.toString();
		
	}
	
	
	
	
	
	private Tokeninfo getAccessTokenFromAuthCode(String authCode){
				
		//set the header parameters
		configurationMap = new HashMap<String, String>();
		configurationMap.put("mode", "sandbox");
		configurationMap.put(Constants.CLIENT_ID, clientId);
		configurationMap.put(Constants.CLIENT_SECRET, clientSecret);
				
		//get the access token
		APIContext apiContext = new APIContext();
		apiContext.setConfigurationMap(configurationMap);
	
		CreateFromAuthorizationCodeParameters param = new CreateFromAuthorizationCodeParameters();
		param.setCode(authCode);
		param.setGrantType("authorization_code");
		param.setRedirectURI("https://devtools-paypal.com");
		
		try {
			tokenInfo = Tokeninfo.createFromAuthorizationCode(apiContext, param);		 
		}catch(PayPalRESTException e) {
				e.printStackTrace();	
		}
	
		return tokenInfo;
	}
	
	
	
	
	
	private Userinfo getUserInfoFromAccessToken(){
		
		APIContext apiContext;
		Userinfo userInfo = null;
		
		try {
			//get the user info  - all the access token permissions will return
			//configurationMap = new HashMap<String, String>();
			//configurationMap.put("mode", "sandbox");

			apiContext = new APIContext();
			apiContext.setConfigurationMap(configurationMap);

			UserinfoParameters param1 = new UserinfoParameters();
			param1.setAccessToken(tokenInfo.getAccessToken());

			userInfo = Userinfo.getUserinfo(apiContext, param1);
		
		} catch (PayPalRESTException e) {
			e.printStackTrace();
		}
		
		return userInfo;
	}
	
	
	
	
	
	
	
	private boolean saveDataToDB(Userinfo userinfo){
		
		String URL = "jdbc:mysql://127.11.254.130:3306/prepay1.0";
		String USERNAME = "adminijH4iQG";
		String PASS = "wij_lXp3HHqj";
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = (Connection) DriverManager.getConnection(URL,USERNAME,PASS);
			Statement stmt = conn.createStatement();
			
			String command = "INSERT INTO `customers`(`account_type`, `user_id`, `private_name`, `last_name`, `user_name`, `password`, `email`, "
					+ "`phone_number`, `street_address`,`country`,`refresh_token`) "
					+ "VALUES ('paypal',"
    				+"'"+userinfo.getUserId()+"',"
    				+"'"+userinfo.getGivenName()+"',"
    				+"'"+userinfo.getFamilyName()+"',"
    				+"'"+userinfo.getEmail()+"',"
    				+"'"+tokenInfo.getAccessToken()+"',"	//the password in DB using to save access token instead of its regular use
    				+"'"+userinfo.getEmail()+"',"
    				+"'"+userinfo.getPhoneNumber()+"',"
    				+"'"+userinfo.getAddress().getStreetAddress()+"',"
    				+"'"+userinfo.getAddress().getCountry()+"',"
    				+"'"+tokenInfo.getRefreshToken()+"')";
			 
		    int rs = stmt.executeUpdate(command);	
		    //TODO: this way of checking is not good
	
		    conn.close();
		    if (rs>0){
		    	return true;
			}
		    
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();	
		}
		
		return true;
	}
	
	
	
	
	
	
	
	
	private String buildMessageToUser(String userName, String sessionPassword, boolean success, String whyNotSucceed) {
		
		JSONObject jsonObject = new JSONObject();
		
		try 
		{
			if (success)
			{
				jsonObject.put("user name", userName);
				jsonObject.put("session password", sessionPassword);	//need to process temporary key, for this session
				jsonObject.put("success", true);
			}
			
			else
			{
				jsonObject.put("success", false);
				jsonObject.put("user name", userName);
				jsonObject.put("session password", "-1");	//need to process temporary key, for this session
				jsonObject.put("why not succeed", whyNotSucceed);
			}
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
			
		return jsonObject.toString();
	}
	
		

}
