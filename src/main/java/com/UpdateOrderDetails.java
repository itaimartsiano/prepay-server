package com;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.mysql.jdbc.Connection;


@WebServlet("/update")
public class UpdateOrderDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    public UpdateOrderDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

    
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("\n new update request");
		
		String userName = "";
		String passwordFromServer= "";
		int version = 0;
		
		InputStream inputstream = request.getInputStream();
		char [] messagefromclient = new char[1024];
		int read = -1;
		String messageToClient = "";
		String nickName = "";
		String restName = "";
 
		StringBuilder sb = new StringBuilder();

		//reading from client
	    try 
	    {
	    	BufferedReader reader = new BufferedReader(new InputStreamReader(inputstream));
	      
	    	while ((read = reader.read(messagefromclient)) > 0)
	    	sb.append(messagefromclient,0,read);
	    } 
	    
	    catch (Exception e) 
	    {
	    	e.printStackTrace();
	    }
		
		
		System.out.println("user message: "+ sb.toString());
		
		try {
			//process the message from client
			
			JSONObject jsonObject = new JSONObject(sb.toString());
			userName = jsonObject.getString("user name");
			passwordFromServer = jsonObject.getString("password from server");
			version = jsonObject.getInt("version");
			nickName = jsonObject.getString("nickName");
			restName = jsonObject.getString("restName");
			
			boolean approved = Security.checkSessionPassword(userName,passwordFromServer);
			if (approved){
				messageToClient = getOrderDetails(nickName, restName, version);
			}
				
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}
		
		//send the message to user
		PrintWriter writer= response.getWriter();
		System.out.println("server message to client: "+ messageToClient);
		response.setStatus(200);	
		writer.append(messageToClient);
		
	}
	

	
	
	
	

	
	
	private String getResultInJson(ResultSet rs){
		
		JSONObject jsonObject = new JSONObject();
		
		try 
		{
			System.out.println("new version");
			
			int version = rs.getInt("version");
			jsonObject.put("version", version);
			
			int numOfProducts = 0;
			
			JSONObject jsonSon = new JSONObject();
            jsonSon.put("product name", rs.getString("name"));
            jsonSon.put("price", rs.getString("price"));
            jsonSon.put("product_order_number", rs.getString("product_order_number"));
            jsonObject.put(String.valueOf(numOfProducts),jsonSon.toString());
            numOfProducts ++;
            
			while (rs.next()){
                jsonSon = new JSONObject();
                jsonSon.put("product name", rs.getString("name"));
                jsonSon.put("price", rs.getString("price"));
                jsonSon.put("product_order_number", rs.getString("product_order_number"));
                jsonObject.put(String.valueOf(numOfProducts),jsonSon.toString());
                numOfProducts ++;
			}
			
			jsonObject.put("num of products", numOfProducts);	//need to process temporary key, for this session
		} 
		
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		catch (SQLException e){
			e.printStackTrace();
		}
	
		
		return jsonObject.toString();
	}
	
	
	
	
	
	
	
	private String getOrderDetails(String nickName, String restName, int version){
		
		String URL = "jdbc:mysql://127.11.254.130:3306/prepay1.0";
		String USERNAME = "adminijH4iQG";
		String PASS = "wij_lXp3HHqj";
		ResultSet rs = null;
		String ansToClient = null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = (Connection) DriverManager.getConnection(URL,USERNAME,PASS);
			
			Statement stmt = conn.createStatement();
			String message = "select `version`, `name`,`price`,`amount`,`product_order_number`,`order_details`.`status` "
					+ "from (`orders` join `order_details` on `orders`.`order_id`=`order_details`.`order_id`) join `products` on `order_details`.`product_id`=`products`.`id`"
					+ " where `order_details`.`status` = 'ACTIVE' and `orders`.`nickname` = '"+nickName+"' and `restuarant_name` = '"+restName+"'";
		    rs = stmt.executeQuery(message);
		    
		    if (rs!=null && rs.next()&& rs.getInt("version") > version){
		    	ansToClient = getResultInJson(rs);
		    }else{
		    	String versionMessage = "select `version` from `orders` where `nickname` = '"+nickName+"' and `restuarant_name` = '"+restName+"'";
			    rs = stmt.executeQuery(versionMessage);
			    rs.next();
			    
		    	JSONObject jsonAns = new JSONObject();
		    	jsonAns.put("version",rs.getInt("version"));
		    	ansToClient = jsonAns.toString();
		    }
		   
		    conn.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();	
		}
		
		return ansToClient;
	}
	
	
	
	


}
