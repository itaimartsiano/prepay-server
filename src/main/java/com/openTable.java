package com;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.mysql.jdbc.Connection;

@WebServlet("/openTable")
public class openTable extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	

    public openTable() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    
    


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		System.out.println("\n new open table request");
		
		String userName = "";
		String passwordFromServer = "";
		String resturantName = "";
		String nickname = "";
		
		InputStream inputstream = request.getInputStream();
		char [] messagefromclient = new char[1024];
		int read = -1;
		String messageToClient = "";
 
		StringBuilder sb = new StringBuilder();

		//reading from client
	    try 
	    {
	    	BufferedReader reader = new BufferedReader(new InputStreamReader(inputstream));
	      
	    	while ((read = reader.read(messagefromclient)) > 0)
	    	sb.append(messagefromclient,0,read);
	    } 
	    
	    catch (Exception e) 
	    {
	    	e.printStackTrace();
	    }
		
		
		System.out.println("user message: "+ sb.toString());
		
		try {
			//process the message from client
			
			JSONObject jsonObject = new JSONObject(sb.toString());
			userName = jsonObject.getString("user name");
			passwordFromServer = jsonObject.getString("password");
			resturantName = jsonObject.getString("restName");
			nickname = jsonObject.getString("nickname");
			
			boolean approved = Security.checkSessionPassword(userName, passwordFromServer);
			
			if (approved){
				messageToClient = buildMessageToUser(openTable(resturantName, nickname,userName));
			}
			else{
				//TODO: add something todo if user is not authrized
			}
			
				
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}
		
		//send the message to user
		PrintWriter writer= response.getWriter();
		System.out.println("server message to client: "+ messageToClient);
		response.setStatus(200);	
		writer.append(messageToClient);
		
	}
	
	
	
	
	private boolean openTable(String restName, String nickname, String userName){
		
		String URL = "jdbc:mysql://127.11.254.130:3306/prepay1.0";
		String USERNAME = "adminijH4iQG";
		String PASS = "wij_lXp3HHqj";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = (Connection) DriverManager.getConnection(URL,USERNAME,PASS);
			Statement stmt = conn.createStatement();
			//TODO: change it - now it takes you directly to the user name table no matter which rest you chose
			//TODO: need to check if you are friend in other table and not just in case you are the owner
			ResultSet res = stmt.executeQuery("select * from `prepay1.0`.`orders` where `customer_owner` = '"+userName+"'");
			if (res.next()){
				String nickName = res.getString("nickname");
				if (nickName.equals(nickname)){
					return true;
				}
				return false;
			}else{
				
				boolean rs = stmt.execute("INSERT INTO `prepay1.0`.`orders` (`order_id`, `customer_owner`, `status`, `restuarant_name`, `nickname`)"
			    		+ " VALUES (NULL, '"+userName+"', 'Active', '"+restName+"', "+"'"+nickname+"')");
				
			    if (!rs){
			    	ResultSet orderIdQuery = stmt.executeQuery("SELECT `order_id` FROM `orders` WHERE `customer_owner` = '"+userName+"'");
			    	String orderId = "";
			    	if (orderIdQuery.next()){
			    		orderId = orderIdQuery.getString(1);
			    	}
			    	
			    	stmt.execute("INSERT INTO `prepay1.0`.`order_partners` (`customer_user_name`, `order_id`) VALUES ('"+userName+"','"+orderId+"')");
			    				    	
			    	return true;
					
				}
			}
			
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();	
		}
		
		return false;
	}
	
	
	
	
	
	
	
	private String buildMessageToUser(boolean approved) {
		
		JSONObject jsonObject = new JSONObject();
		
		try 
		{
			if (approved)
			{
				System.out.println("approved");
				jsonObject.put("approved", true);
			}
			
			else
			{
				System.out.println("not approved");
				jsonObject.put("approved", false);
			}
		} 
		
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
		
		return jsonObject.toString();
		
	}
	
	
	
	
	

}
