package com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.mysql.jdbc.Connection;


@WebServlet("/getRestList")
public class getRestList extends HttpServlet {

	private static final long serialVersionUID = 1L;
       
   
    public getRestList() {
        super();
        // TODO Auto-generated constructor stub
    }

	

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
		System.out.println("\n new get rest list request");
		
		String userName = "";
		String passwordFromServer = "";
		
		InputStream inputstream = request.getInputStream();
		char [] messagefromclient = new char[1024];
		int read = -1;
		String messageToClient = "";
 
		StringBuilder sb = new StringBuilder();

		//reading from client
	    try 
	    {
	    	BufferedReader reader = new BufferedReader(new InputStreamReader(inputstream));
	      
	    	while ((read = reader.read(messagefromclient)) > 0)
	    	sb.append(messagefromclient,0,read);
	    } 
	    
	    catch (Exception e) 
	    {
	    	e.printStackTrace();
	    }
		
		
		System.out.println("user message: "+ sb.toString());
		
		try {
			//process the message from client
			
			JSONObject jsonObject = new JSONObject(sb.toString());
			userName = jsonObject.getString("user name");
			passwordFromServer = jsonObject.getString("password");
			
			boolean approved = Security.checkSessionPassword(userName, passwordFromServer);
			
			if (approved){
				messageToClient = getRestList().toString();
			}
			else{
				//TODO: add something todo if user is not authrized
			}
			
				
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}
		
		//send the message to user
		PrintWriter writer= response.getWriter();
		System.out.println("server message to client: "+ messageToClient);
		response.setStatus(200);	
		writer.append(messageToClient);
		
	}
	
	
	
	
	private  JSONObject getRestList(){
		
		String URL = "jdbc:mysql://127.11.254.130:3306/prepay1.0";
		String USERNAME = "adminijH4iQG";
		String PASS = "wij_lXp3HHqj";
		JSONObject jsonMessage = null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = (Connection) DriverManager.getConnection(URL,USERNAME,PASS);
			Statement stmt = conn.createStatement();
			//TODO: change the table to get username and resturant name instead of their ID's
		    
			ResultSet rs = stmt.executeQuery("SELECT `name` FROM `restuarants`");

			jsonMessage = convertResSetToJson(rs);
			jsonMessage.put("approved", true);
			
			conn.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();	
		}
		
		return jsonMessage;
	}
	
	
	
	
	private JSONObject convertResSetToJson(ResultSet rs){
		
		JSONObject jsonMessage = new JSONObject();
		int i = 0;
		
		try {
			while (rs.next()) {
				String param = rs.getString(1);
				jsonMessage.put("p"+i, param);
				i++;
			}
			jsonMessage.put("length", i);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		
		return jsonMessage;
		
	}
	
	
	


}
