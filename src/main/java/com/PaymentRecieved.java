package com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.postgresql.translation.messages_bg;

import com.mysql.jdbc.Connection;


@WebServlet("/paymentRecieved")
public class PaymentRecieved extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public PaymentRecieved() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	System.out.println("\n new Payment request");
		
		String userName = "";
		String passwordFromServer= "";
		String tableNickName = "";
		String restName = "";
		String productPayedNumbers = "";
		
		InputStream inputstream = request.getInputStream();
		char [] messagefromclient = new char[1024];
		int read = -1;
		String messageToClient = "";
		
 
		StringBuilder sb = new StringBuilder();

		//reading from client
	    try 
	    {
	    	BufferedReader reader = new BufferedReader(new InputStreamReader(inputstream));
	      
	    	while ((read = reader.read(messagefromclient)) > 0)
	    	sb.append(messagefromclient,0,read);
	    } 
	    
	    catch (Exception e) 
	    {
	    	e.printStackTrace();
	    }
		
		
		System.out.println("user message: "+ sb.toString());
		
		try {
			//process the message from client
			
			JSONObject jsonObject = new JSONObject(sb.toString());
			userName = jsonObject.getString("user name");
			passwordFromServer = jsonObject.getString("password");
			restName = jsonObject.getString("restName");
			tableNickName = jsonObject.getString("table nickname");
			productPayedNumbers = jsonObject.getString("product payed numbers");
			
			boolean approved = Security.checkSessionPassword(userName,passwordFromServer);
			if (approved){
				messageToClient = markProductsAsPayed(productPayedNumbers,tableNickName,restName);
			}
				
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}
		
		//send the message to user
		PrintWriter writer= response.getWriter();
		System.out.println("server message to client: "+ messageToClient);
		response.setStatus(200);	
		writer.append(messageToClient);
		
	}
	
	
	
	
	
	
	
	private String markProductsAsPayed(String productPayedNumbers, String tableNickname, String restName){
		
		String URL = "jdbc:mysql://127.11.254.130:3306/prepay1.0";
		String USERNAME = "adminijH4iQG";
		String PASS = "wij_lXp3HHqj";
		ResultSet rs = null;
		String ansToClient = null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = (Connection) DriverManager.getConnection(URL,USERNAME,PASS);
			
			String [] productNumbers = productPayedNumbers.split("-");
			
			Statement stmt = conn.createStatement();
			String message = "select `product_order_number`,`order_details`.`status`,`restuarant_name`,`orders`.`order_id`,`version` "
					+ "from (`orders` join `order_details` on `orders`.`order_id`=`order_details`.`order_id`)"
					+ " where `order_details`.`status` = 'ACTIVE' and `nickname` = '"+tableNickname+"' and `restuarant_name` = '"+restName+"'";
		    
			rs = stmt.executeQuery(message);
		   	rs.next();
		   	
		    if (allProductsPayed(rs)){
		    	//call method to take all the reservation to history
		    	////////
		    }else{
		    	long orderId = rs.getLong("order_id");
		    	int version = rs.getInt("version");
		    	markProducts(productNumbers, orderId, stmt);
		    	updateVersion(version, stmt, orderId);
		    }
		    
		    conn.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();	
		}
		
		return getMessageToUser();
	}
	
	
	
	
	
	
	private boolean allProductsPayed(ResultSet rs){
		if (rs != null){
			return false;
		}
		return true;
	}
	
	
	
	
	
	
	private void markProducts(String [] productsNumber, long orderId, Statement statement){
		
		try {
			int rs;
			System.out.println("order ID:---------------"+orderId);
			for (int i=0; i<productsNumber.length; i++){
				String message = "UPDATE `order_details` SET `status`='PAYED' WHERE `order_id`='"+orderId+"' and"
						+ " `product_order_number` = '"+productsNumber[i]+"'";
					rs = statement.executeUpdate(message);	
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
	}
	
	
	private void updateVersion(int version, Statement stmt, long orderId) {
		
		try {
			
				version++;
			String message = "UPDATE `orders` SET `version`='"+version+"' WHERE `order_id`='"+orderId+"'";
			int rs = stmt.executeUpdate(message);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	
	private String getMessageToUser(){
		
		JSONObject jsonObject = new JSONObject();
		
		try 
		{		
			jsonObject.put("success", true);
		}
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
			
		return jsonObject.toString();
	}
	
	
}
